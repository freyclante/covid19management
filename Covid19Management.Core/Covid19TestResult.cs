﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Covid19Management.Core
{
    public class Covid19TestResult
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "testTime")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Time of test")]
        public DateTime TestTime { get; set; }
        [JsonProperty(PropertyName = "isCovid19Positive")]
        public TestOutCome IsCovid19Positive { get; set; }
    }
}