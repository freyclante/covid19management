namespace Covid19Management.Core
{
    public enum YesOrNo
    {
        No = 0,
        Yes = 1
    }
}