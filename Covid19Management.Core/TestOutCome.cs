namespace Covid19Management.Core
{
    public enum TestOutCome
    {
       Negative = 0,
       Positive = 1
    }
}