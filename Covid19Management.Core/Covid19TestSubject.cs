﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Covid19Management.Core
{
    public class Covid19TestSubject
    {
        [JsonProperty(PropertyName = "id")] 
        public string Id { get; set; }

        [Required, StringLength(30)]
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }
        [Required, StringLength(30)]
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [JsonIgnore]
        public string FullName
        {
            get
            {
                return LastName + ", " + FirstName;
            }
            
            private set { }
        }
        [Required]
        [RegularExpression("\\d{6}-\\d{4}")]
        [JsonProperty(PropertyName = "cpr")] 
        public string Cpr { get; set; }

        [JsonProperty(PropertyName = "isTestCompleted")]
        public YesOrNo IsTestCompleted { get; set; }

        [JsonProperty(PropertyName = "testResult")]
        public Covid19TestResult TestResult { get; set; }
    }
}