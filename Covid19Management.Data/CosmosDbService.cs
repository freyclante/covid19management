﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Covid19Management.Core;
using Microsoft.Azure.Cosmos;
using Container = Microsoft.Azure.Cosmos.Container;

namespace Covid19Management.Data
{
    public class CosmosDbService : ICosmosDbService
    {
        private readonly Container _container;

        public CosmosDbService(
            CosmosClient dbClient,
            string databaseName,
            string containerName)
        {
            this._container = dbClient.GetContainer(databaseName, containerName);
        }

        public async Task<Covid19TestSubject> AddItemAsync(Covid19TestSubject covid19TestSubject)
        {
            covid19TestSubject.Id = Guid.NewGuid().ToString();
            var createdSubject = await this._container.CreateItemAsync<Covid19TestSubject>(covid19TestSubject,
                new PartitionKey(covid19TestSubject.Id));
            return createdSubject;
        }

        public async Task DeleteItemAsync(string id)
        {
            await this._container.DeleteItemAsync<Covid19TestSubject>(id, new PartitionKey(id));
        }

        public IEnumerable<Covid19TestSubject> GetSubjectByNameAsync(string name)
        {
            var results = this.GetItemsAsync("SELECT * FROM c").Result.ToList();

            if (name != null)
            {
                return results.Where(ts =>
                {
                    var fullName = $"{ts.FirstName}  {ts.LastName}";
                    return fullName.Contains(name);
                }).Select(ts => ts);
            }

            return results;
        }

        public async Task<Covid19TestSubject> GetItemAsync(string id)
        {
            try
            {
                ItemResponse<Covid19TestSubject> response =
                    await this._container.ReadItemAsync<Covid19TestSubject>(id, new PartitionKey(id));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
        }

        public async Task<IEnumerable<Covid19TestSubject>> GetItemsAsync(string queryString)
        {
            var query = this._container.GetItemQueryIterator<Covid19TestSubject>(new QueryDefinition(queryString));
            var results = new List<Covid19TestSubject>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();

                results.AddRange(response.ToList());
            }

            return results;
        }

        public async Task UpdateItemAsync(string id, Covid19TestSubject covid19TestSubject)
        {
            await this._container.UpsertItemAsync<Covid19TestSubject>(covid19TestSubject, new PartitionKey(id));
        }
    }
}