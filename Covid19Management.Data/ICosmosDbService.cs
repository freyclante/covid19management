﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Covid19Management.Core;

namespace Covid19Management.Data
{
    public interface ICosmosDbService
    {
        Task<IEnumerable<Covid19TestSubject>> GetItemsAsync(string query);
        Task<Covid19TestSubject> GetItemAsync(string id);
        Task<Covid19TestSubject> AddItemAsync(Covid19TestSubject item);
        Task UpdateItemAsync(string id, Covid19TestSubject item);
        Task DeleteItemAsync(string id);
        IEnumerable<Covid19TestSubject> GetSubjectByNameAsync(string name);
    }
}