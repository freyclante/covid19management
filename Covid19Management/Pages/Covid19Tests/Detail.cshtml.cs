using System.Linq;
using Covid19Management.Core;
using Covid19Management.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Covid19Management.Pages.Covid19Tests
{
    public class Detail : PageModel
    {
        private readonly ICosmosDbService _service;
        [TempData]
        public string Message { get; set; }
        public Covid19TestSubject TestSubject { get; set; }
        public Detail(ICosmosDbService service)
        {
            _service = service;
        }
        public IActionResult OnGet(string subjectId)
        {
            TestSubject = _service.GetItemsAsync("SELECT * FROM c").Result.
                SingleOrDefault(ts => ts.Id.Equals(subjectId));
            if (TestSubject == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}