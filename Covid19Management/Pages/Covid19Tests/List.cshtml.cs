using System;
using System.Collections.Generic;
using Covid19Management.Core;
using Covid19Management.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Covid19Management.Pages.Covid19Tests
{
    public class List : PageModel
    {
        private readonly IConfiguration _config;
        private readonly ICosmosDbService _service;
        public IEnumerable<Covid19TestSubject> TestSubjects { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string SearchTerm { get; set; }
        
        public List(IConfiguration config, ICosmosDbService service)
        {
            _config = config;
            _service = service;
        }

        public void OnGet(string searchTerm)
        {
             TestSubjects = _service.GetSubjectByNameAsync(SearchTerm);
             Console.WriteLine(TestSubjects);
        }
    }
}