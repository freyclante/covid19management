using Covid19Management.Core;
using Covid19Management.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Covid19Management.Pages.Covid19Tests
{
    public class Delete : PageModel
    {
        private readonly ICosmosDbService _service;
        public Covid19TestSubject TestSubject { get; set; }

        public Delete(ICosmosDbService service)
        {
            _service = service;
        }

        public void OnGet(string subjectId)
        {
            TestSubject = _service.GetItemAsync(subjectId).Result;
        }

        public IActionResult OnPost(string subjectId)
        {
            _service.DeleteItemAsync(subjectId);
            return RedirectToPage("./List");
        }
    }
}