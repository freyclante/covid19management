using System;
using System.Collections.Generic;
using System.Text;
using Covid19Management.Core;
using Covid19Management.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Covid19Management.Pages.Covid19Tests
{
    public class Edit : PageModel
    {
        private readonly ICosmosDbService _service;
        private readonly IHtmlHelper _htmlHelper;
        [BindProperty] public Covid19TestSubject TestSubject { get; set; }
        public IEnumerable<SelectListItem> TestOutComes { get; set; }
        public IEnumerable<SelectListItem> TestCompletedAnswers { get; set; }

        public Edit(ICosmosDbService service, IHtmlHelper htmlHelper)
        {
            _service = service;
            _htmlHelper = htmlHelper;
        }

        public IActionResult OnGet(string subjectId)
        {
            TestCompletedAnswers = _htmlHelper.GetEnumSelectList<YesOrNo>();
            TestOutComes = _htmlHelper.GetEnumSelectList<TestOutCome>();
            if (!string.IsNullOrEmpty(subjectId))
            {
                TestSubject = _service.GetItemAsync(subjectId).Result;
                var bytes = Convert.FromBase64String(TestSubject.Cpr);
                TestSubject.Cpr = Encoding.UTF8.GetString(bytes);
            }
            else
            {
                TestSubject = new Covid19TestSubject();
            }

            if (TestSubject == null)
            {
                return RedirectToPage("./NotFound");
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                TestCompletedAnswers = _htmlHelper.GetEnumSelectList<YesOrNo>();
                TestOutComes = _htmlHelper.GetEnumSelectList<TestOutCome>();
                return Page();
            }

            if (!string.IsNullOrEmpty(TestSubject.Id))
            {
                if (TestSubject.TestResult.IsCovid19Positive.Equals(TestOutCome.Positive))
                {
                    TestSubject.IsTestCompleted = YesOrNo.Yes;
                    TestSubject.TestResult.TestTime = DateTime.Now;
                }
                TestSubject.Cpr = Convert.ToBase64String(Encoding.UTF8.GetBytes(TestSubject.Cpr),
                    Base64FormattingOptions.None);
                _service.UpdateItemAsync(TestSubject.Id, TestSubject);
            }
            else
            {
                if (TestSubject.IsTestCompleted.Equals(YesOrNo.Yes))
                {
                    TestSubject.TestResult.TestTime = DateTime.Now;
                }

                TestSubject.Cpr = Convert.ToBase64String(Encoding.UTF8.GetBytes(TestSubject.Cpr),
                    Base64FormattingOptions.None);
                TestSubject = _service.AddItemAsync(TestSubject).Result;
            }

            TempData["Message"] = "Test Subject Saved!";
            return RedirectToPage("./Detail", new {subjectId = TestSubject.Id});
        }
    }
}