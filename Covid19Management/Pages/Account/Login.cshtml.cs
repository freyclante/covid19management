using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Covid19Management.Pages.Account
{
    public class Login : PageModel
    {
        public async Task OnGet(string returnUrl = "./Covid19Tests/List")
        {
            await HttpContext.ChallengeAsync("Auth0", new AuthenticationProperties() { RedirectUri = returnUrl });
        }
    }
}