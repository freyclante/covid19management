using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Covid19Management.Security
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseSecurityHeaders(this IApplicationBuilder app)
        {
            app.UseMiddleware<SecurityHeadersMiddleware>();
        }
    }
    public class SecurityHeadersMiddleware
    {
        private readonly RequestDelegate _next;

        public SecurityHeadersMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        
        public async Task Invoke(HttpContext context)
        {
            //context.Response.Headers.Add("X-XSS-Protection", "1; mode=block"); -- only for browsers that does not support Content Security Policies.
            context.Response.Headers.Add("Content-Security-Policy",
                "style-src 'self' https://kit-free.fontawesome.com/releases/latest/css/free.min.css " +
                "https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css " +
                "https://kit-free.fontawesome.com/releases/latest/css/free-v4-font-face.min.css " +
                "https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css;" +
                "frame-ancestors 'none'");

            context.Response.Headers.Add("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
            context.Response.Headers.Add("Referrer-Policy", "no-referrer");
            context.Response.Headers.Add("Permissions-Policy", "geolocation=(self https://example.com), microphone=(), camera=()");
            
            await _next(context);
        }
    }
}