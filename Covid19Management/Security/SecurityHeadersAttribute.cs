using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Covid19Management.Security
{
    public class SecurityHeadersAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext context)
        {
            var result = context.Result;

            if (result is PageResult)
            {
                context.HttpContext.Response.Headers
                    .Add("", "");
            }
        }
    }
}